# taia-prompts

Code generation (mainly .cljc) using the Clojure REPL with predefined templates.

A video is here: https://youtu.be/e1ZvjCwuxYk

### Sample

For pure.clj, using LM Studio, CodeLLama 7B:


I have the following code
```
(ns cljc.fns.pure.sample-1)

(defn select-reason-id [ctx {:keys [reason-id]}]
  (assoc-in ctx [:selected :reason-id] reason-id)) 
```                    
For it I wrote the following test using hyperfiddle.rcf/tests macro: 
   
```
(ns cljc.fns.pure.sample-1-test
  (:require 
   [hyperfiddle.rcf :refer [tests]]
   [cljc.fns.pure.sample-1 :refer [select-reason-id]]))

(tests 
 ;;when it didn't exist and none provided
 (select-reason-id {} {}) := {:selected {:reason-id nil}}
 ;;when it existed before and none provided
 (select-reason-id {:selected {:reason-id 5}} {}) := {:selected {:reason-id nil}}
 
 ;;when it didn't exist
 (select-reason-id {} {:reason-id 5}) := {:selected {:reason-id 5}}
 ;;when it existed before
 (select-reason-id {:selected {:reason-id 5}} {:reason-id 6}) := {:selected {:reason-id 6}}
 
 )
```

Can you to write the test or tests for the following function, also using hyperfiddle.rcf/tests macro
   
```
(ns cljc.fns.pure.sample-2)

(defn deselect-reason-id [ctx _]
  (if (:selected ctx)
    (update ctx :selected #(dissoc % :reason-id))
    ctx)) 
```

?   

**Response**

Sure, here are the tests for the `deselect-reason-id` function:
```
(ns cljc.fns.pure.sample-2-test
  (:require [hyperfiddle.rcf :refer [tests]]
            [cljc.fns.pure.sample-2 :refer [deselect-reason-id]]))

(tests
 ;; when there is no selected reason id
 (deselect-reason-id {} nil) := {:selected {}}
 ;; when there is a selected reason id
 (deselect-reason-id {:selected {:reason-id 5}} nil) := {:selected {}}
 ;; when there is no selected reason id and the context has other keys
 (deselect-reason-id {:a 1 :b 2} nil) := {:a 1 :b 2}
 ;; when there is a selected reason id and the context has other keys
 (deselect-reason-id {:selected {:reason-id 5} :a 1 :b 2} nil) := {:a 1 :b 2}
)
```





* Tests *

```
bb test
```