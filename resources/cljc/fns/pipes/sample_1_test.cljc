(ns cljc.fns.pipes.sample-1-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [cljc.fns.pipes.sample-1 :refer [code->token+user-info]]
   [cljc.fns.side-effects.sample-1 :refer [assoc-access-token-using-code]]
   [cljc.fns.side-effects.sample-2 :refer [assoc-user-info-using-access-token]]
   [spy.core :as spy]))

(tests
 (defn pipe [step]
   (fn [a & _]
     (if (:pipe a)
       (update a :pipe conj step)
       (assoc a :pipe [step]))))

 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})

 (def request {:code :code
               :config {:domain "domain"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}
               :tokens tokens})

 (with-redefs [assoc-access-token-using-code      (spy/spy (pipe :assoc-access-token-using-code))
               assoc-user-info-using-access-token (spy/spy (pipe :assoc-user-info-using-access-token))]
   (code->token+user-info request) := (merge request {:pipe [:assoc-access-token-using-code
                                                             :assoc-user-info-using-access-token]})

   (spy/called-once-with? assoc-access-token-using-code request) := true
   (spy/called-once-with? assoc-user-info-using-access-token (merge request {:pipe [:assoc-access-token-using-code]})) := true))

