(ns cljc.fns.pipes.sample-2-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [cljc.fns.pipes.sample-2 :refer [execute step-1 step-2 step-3]]
   [spy.core :as spy]))

(tests
 (defn pipe [step]
   (fn [a & _]
     (if (:pipe a)
       (update a :pipe conj step)
       (assoc a :pipe [step]))))

 (def initial-context {:key "value"}) ; Replace with your actual initial context

 (with-redefs [step-1 (spy/spy (pipe :step-1))
               step-2 (spy/spy (pipe :step-2))
               step-3 (spy/spy (pipe :step-3))]
   (execute initial-context) := (merge initial-context {:pipe [:step-1
                                                               :step-2
                                                               :step-3]})

   (spy/called-once-with? step-1 initial-context) := true
   (spy/called-once-with? step-2 (merge initial-context {:pipe [:step-1]})) := true
   (spy/called-once-with? step-3 (merge initial-context {:pipe [:step-1 :step-2]})) := true))


