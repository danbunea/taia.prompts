(ns cljc.fns.pipes.sample-2)

(defn step-1 [_ctx])
(defn step-2 [_ctx])
(defn step-3 [_ctx])

(defn execute [ctx]
  (-> ctx
      step-1
      step-2
      step-3))

