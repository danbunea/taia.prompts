(ns cljc.fns.pipes.sample-1
  (:require
   [cljc.fns.side-effects.sample-1 :refer [assoc-access-token-using-code]]
   [cljc.fns.side-effects.sample-2 :refer [assoc-user-info-using-access-token]]))

(defn code->token+user-info [{:keys [code config] :as ctx}]
  (-> ctx
      assoc-access-token-using-code
      assoc-user-info-using-access-token))

