(ns cljc.fns.pure.sample-1)

(defn select-reason-id [ctx {:keys [reason-id]}]
  (assoc-in ctx [:selected :reason-id] reason-id))