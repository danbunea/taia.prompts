(ns cljc.fns.pure.sample-1-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [cljc.fns.pure.sample-1 :refer [select-reason-id]]))

(tests
 ;;when it didn't exist and none provided
 (select-reason-id {} {}) := {:selected {:reason-id nil}}
 ;;when it existed before and none provided
 (select-reason-id {:selected {:reason-id 5}} {}) := {:selected {:reason-id nil}}

 ;;when it didn't exist
 (select-reason-id {} {:reason-id 5}) := {:selected {:reason-id 5}}
 ;;when it existed before
 (select-reason-id {:selected {:reason-id 5}} {:reason-id 6}) := {:selected {:reason-id 6}})