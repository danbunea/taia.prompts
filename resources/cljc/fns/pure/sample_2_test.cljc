(ns cljc.fns.pure.sample-2-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [cljc.fns.pure.sample-2 :refer [deselect-reason-id]]))

(tests
 ;;when it didn't exist and none provided
 (deselect-reason-id {} nil) := {}
 ;;when it existed before and none provided
 (deselect-reason-id {:selected {:reason-id 5 :some-val :a}} nil) := {:selected {:some-val :a}})