(ns cljc.fns.pure.sample-2)

(defn deselect-reason-id [ctx _]
  (if (:selected ctx)
    (update ctx :selected #(dissoc % :reason-id))
    ctx))