(ns cljc.fns.side-effects.sample-2
  (:require
   [clj-http.client :as client]))

(defn unexceptional-status [code] (<= 200 code 499))

(defn assoc-user-info-using-access-token [{:keys [tokens config] :as ctx}]
  (let [user-info-response (client/get (str "https://" (:domain config) "/userinfo") {:headers {"Authorization" (str "Bearer " (:acces-token tokens))}
                                                                                      :unexceptional-status unexceptional-status
                                                                                      :as :json})]

    (if (= 200 (:status user-info-response))
      (assoc ctx :user-info (:body user-info-response))
      ;;else
      (throw (ex-info "Auth Error: User info not obtained based on access-token"
                      {:type :auth/acces-token-error
                       :data (select-keys user-info-response [:status :body])})))))







