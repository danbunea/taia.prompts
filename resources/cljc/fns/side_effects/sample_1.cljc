(ns cljc.fns.side-effects.sample-1
  (:require
   [clj-http.client :as client]))

(defn unexceptional-status [code] (<= 200 code 499))

(defn assoc-access-token-using-code [{:keys [code config] :as ctx}]
  (let [auth0-token-response (client/post (str "https://" (:domain config) "/oauth/token")
                                          {:form-params {:client_id     (:client-id config)
                                                         :client_secret (:client-secret  config)
                                                         :code          code
                                                         :grant_type    "authorization_code"
                                                         :redirect_uri  (:redirect-uri  config)}
                                           :unexceptional-status unexceptional-status
                                           :as :json})]
    (if (= 200 (:status auth0-token-response))
      (-> ctx
          (assoc-in [:tokens :acces-token] (get-in auth0-token-response [:body :access_token]))
          (assoc-in [:tokens :id-token] (get-in auth0-token-response [:body :id_token]))
          (assoc-in [:tokens :expires-in] (get-in auth0-token-response [:body :expires_in])))

      (throw (ex-info "Auth Error: Cound not obtain access token using code"
                      {:type :auth/code-error
                       :data (select-keys auth0-token-response [:status :body])})))))





