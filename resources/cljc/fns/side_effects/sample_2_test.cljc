(ns cljc.fns.side-effects.sample-2-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [clj-http.client :as client]
   [cljc.fns.side-effects.sample-2 :refer [assoc-user-info-using-access-token unexceptional-status]]
   [spy.core :as spy]))

(tests
 ;;happy path
 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})
 (def request {:code :code
               :config {:domain "domain"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}
               :tokens tokens})
 (def params-http {:headers {"Authorization" (str "Bearer " (:acces-token tokens))}
                   :unexceptional-status unexceptional-status
                   :as :json})
 (with-redefs [client/get (spy/spy (fn [_ _] {:status 200 :body {:user-id 1}}))]
   (assoc-user-info-using-access-token request) := (merge request {:user-info {:user-id 1}})
   (spy/called-once-with? client/get "https://domain/userinfo" params-http) := true)

 (with-redefs [client/get (spy/spy (fn [_ _] {:status 403 :body {:error "Error"}}))]
   (assoc-user-info-using-access-token request) :throws clojure.lang.ExceptionInfo
   (spy/called-once-with? client/get "https://domain/userinfo" params-http) := true))
