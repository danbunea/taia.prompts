(ns cljc.fns.side-effects.sample-1-test
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [clj-http.client :as client]
   [cljc.fns.side-effects.sample-1 :refer [assoc-access-token-using-code unexceptional-status]]
   [spy.core :as spy]))

(tests
 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})
 (def request {:code :code
               :config {:domain "domain"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}})
 (def params-http {:form-params {:client_id     :client-id
                                 :client_secret :client-secret
                                 :code          :code
                                 :grant_type    "authorization_code"
                                 :redirect_uri  :redirect-uri}
                   :unexceptional-status unexceptional-status
                   :as :json})
 ;;happy path
 (with-redefs [client/post (spy/spy (fn [_ _] {:status 200 :body {:access_token :access-token
                                                                  :id_token :id-token
                                                                  :expires_in :expires-in}}))]
   (assoc-access-token-using-code request) := (merge request {:tokens tokens})
   (spy/called-once-with? client/post "https://domain/oauth/token" params-http) := true)

 (with-redefs [client/post (spy/spy (fn [_ _] {:status 403 :body {:error "Error"}}))]
   (assoc-access-token-using-code request) :throws clojure.lang.ExceptionInfo
   (spy/called-once-with? client/post "https://domain/oauth/token" params-http) := true))
