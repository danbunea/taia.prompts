(ns prompts.cljc.fns.fns)

(def fn-gen-test-template
  "I have the following function written in Clojure 
{{fn}}. 
                       
For it I wrote the following test using hyperfiddle.rcf/tests macro: 
   
{{fn-test}}. 

Can you to write the test or tests for the following function, also using hyperfiddle.rcf/tests macro
   
{{new-fn}} 
   
?")

(def fn-gen-code-template
  "I wrote the following test using Clojure using hyperfiddle.rcf/tests macro : 
   
{{fn-test}}. 
   
To make the tests pass, I wrote the following code in Elixir 
   
{{fn}}.                 

Can you to write the code, that will make the following tests using hyperfiddle.rcf/tests macro 
   
{{new-tests}} 
   
pass?")
