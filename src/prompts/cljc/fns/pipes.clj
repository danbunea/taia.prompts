(ns prompts.cljc.fns.pipes
  (:require
   [bosquet.llm.generator :as g]
   [prompts.prompt :refer [chatgpt-prompt fill-slots-from-files]]
   [bosquet.wkk :as wkk]
   [prompts.source :refer [read-source-file]]
   [prompts.cljc.fns.fns :refer [fn-gen-code-template fn-gen-test-template]]))

(defn update-with-file-content [file-name]
  (read-source-file (str "cljc/fns/pipes/" file-name ".cljc")))

(defn slots-test [sample-code-file sample-test-file code-file]
  (fill-slots-from-files {:fn sample-code-file
                          :fn-test sample-test-file
                          :new-fn code-file}
                         update-with-file-content))

(defn slots-code [sample-test-file sample-code-file tests-file]
  (fill-slots-from-files {:fn sample-code-file
                          :fn-test sample-test-file
                          :new-tests tests-file}
                         update-with-file-content))

(comment
  (g/generate fn-gen-test-template (slots-test "sample_1" "sample_1_test" "sample_2") {:answer {wkk/service :llm/lmstudio}})
  (g/generate fn-gen-code-template (slots-code "sample_1" "sample_1_test" "sample_2") {:answer {wkk/service :llm/lmstudio}})

;; ChatGPT prompt generation
  (println (chatgpt-prompt
            fn-gen-test-template
            (slots-test "sample_1" "sample_1_test" "sample_2")))

  (println (chatgpt-prompt
            fn-gen-test-template
            (slots-test "sample_2" "sample_2_test" "sample_1")))

  (println (chatgpt-prompt
            fn-gen-code-template
            (slots-code "sample_1_test"  "sample_1" "sample_2_test")))

  nil)