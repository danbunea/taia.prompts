(ns prompts.source
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [clojure.java.io :as io]))

(defn read-source-file [file-name]
  (let [resource-url (io/resource file-name)
        line-separator (System/getProperty "line.separator")]
    (when resource-url
      (with-open [reader (io/reader resource-url)]
        (clojure.string/join line-separator (doall (line-seq reader)))))))

(tests
 (read-source-file "cljc/fns/pure/sample_1.cljc") := "(ns cljc.fns.pure.sample-1)\n\n(defn select-reason-id [ctx {:keys [reason-id]}]\n  (assoc-in ctx [:selected :reason-id] reason-id))")