(ns prompts.prompt
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [bosquet.template.read :as template]))

(defn chatgpt-prompt [template slots]
  (-> (template/fill-slots
       template
       slots
       {})
      first))

(tests
 (chatgpt-prompt "First {{one}}", {:one 1}) := "First 1"
 (chatgpt-prompt "First {{one}} \n {{two}}", {:one 1 :two "two"}) := "First 1 \n two")

(defn fill-slots-from-files [slots update-fn]
  (->> slots
       (reduce
        (fn [acc [slot-key file-path]]
          (assoc acc slot-key (update-fn  file-path)))
        {})
       (into {})))

(tests
 (fill-slots-from-files {:fn "sample_1"
                         :fn-test "sample_1_test"
                         :new-fn "sample_2"}
                        #(str "cljc/fns/side_effects/" % ".cljc"))
 *1 := {:fn "cljc/fns/side_effects/sample_1.cljc"
        :fn-test "cljc/fns/side_effects/sample_1_test.cljc"
        :new-fn "cljc/fns/side_effects/sample_2.cljc"}

 (fill-slots-from-files {:fn-test "sample_1_test"
                         :fn "sample_1"
                         :new-test "sample_2_test"}
                        #(str "cljc/fns/side_effects/" % ".cljc"))
 *1 := {:fn "cljc/fns/side_effects/sample_1.cljc"
        :fn-test "cljc/fns/side_effects/sample_1_test.cljc"
        :new-test "cljc/fns/side_effects/sample_2_test.cljc"}

 (fill-slots-from-files {:fn-1 "sample_1"
                         :fn-1-test "sample_1_test"
                         :fn-2 "sample_2"
                         :fn-2-test "sample_2_test"
                         :code "sample"}
                        #(str "cljc/fns/side_effects/" % ".cljc"))
 *1 := {:fn-1 "cljc/fns/side_effects/sample_1.cljc"
        :fn-1-test "cljc/fns/side_effects/sample_1_test.cljc"
        :fn-2 "cljc/fns/side_effects/sample_2.cljc"
        :fn-2-test "cljc/fns/side_effects/sample_2_test.cljc"
        :code "cljc/fns/side_effects/sample.cljc"})